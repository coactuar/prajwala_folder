function validateform(){  
    var first_name = 
        document.forms["myForm"]["first_name"];  
    var last_name = 
        document.forms["myForm"]["last_name"];   
    var oraganization = 
        document.forms["myForm"]["oraganization"]; 
    var city = 
        document.forms["myForm"]["city"]; 
    var mobile_number = 
        document.forms["myForm"]["mobile_number"]; 
    var email = 
        document.forms["myForm"]["email"]; 

    if (first_name.value == ""){  
         window.alert("First Name can't be blank");  
        return false;  
    }  

    if (last_name.value == ""){  
         window.alert("Last Name can't be blank");  
        return false;  
    }

    if(oraganization.value == ""){  
         window.alert("oraganization can't be blank");  
        return false;  
    }  

    if(city.value == ""){  
         window.alert("city can't be blank");  
        return false;  
    }  

    if(mobile_number.value == ""){  
         window.alert("mobile_number can't be blank");  
        return false;  
    } 

    if(email.value == ""){  
         window.alert("email can't be blank");  
        return false;  
    } 

    return true; 
}  

function validateLoginForm() {
    var email = 
    document.forms["myForm"]["email"]; 
    var password = 
        document.forms["myForm"]["password"]; 
    
    if(email.value == ""){  
        window.alert("email can't be blank");  
        return false;  
    } 

    if(password.value == "" || password.length < 6){  
        window.alert("Password must be at least 6 characters long.");  
        return false;  
    }   

    return true;
}